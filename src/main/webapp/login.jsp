<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登录页</title>
</head>
<body>

	<h4>Login Page</h4>
	
	<form action="${APP_PATH}/user/login" method="post">
		username: <input type="text" name="username" title="用户名"/>
		<br><br>
		
		password: <input type="password" name="password" title="密码"/>
		<br><br>
		
		rememberMe: <input type="checkbox" name="remember-me" title="记住我"/>
		<br><br>
		
		<!-- csrf隐藏域 -->
		<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }"/>
		
		<input type="submit" value="Submit"/>
	</form>
	
</body>
</html>