package com.zx.springsecurity.entity;

import lombok.Data;

/**
 * 用户
 * @author 郑雪
 * @date 2021-11-25
 */
@Data
public class Users {

	/**
	 * 用户id
	 */
	private Integer id;
	
	/**
	 * 用户名
	 */
	private String username;
	
	/**
	 * 密码
	 */
	private String password;
}
