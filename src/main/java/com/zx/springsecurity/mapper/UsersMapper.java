package com.zx.springsecurity.mapper;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zx.springsecurity.entity.Users;

/**
 * 用户Mapper
 * 继承了mybatis-plus的BaseMapper，它封装了通用的增删改查
 * 注意由于该类不用自己写实现类，所以需要类上加@Repository注解
 * @author 郑雪
 * @date 2021-11-25
 */
@Repository
public interface UsersMapper extends BaseMapper<Users> {

}
