本项目为SpringMVC集成Spring Security示例
Maven项目 + spring + springmvc + mybatis-plus + spring-security + lombok

Spring Security配置文件 
 目前该项目使用的是配置类的方式，需要把该文件删掉，我就改名为springdel-security.xml，是为了让web.xml找不到它
  如果想使用xml方式配置，需要把该文件名改为spring-security.xml，然后删除SecurityConfig.java